# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require github [ user=mcabber release=${PV} suffix=tar.bz2 ]
require autotools [ supported_autoconf=[ 2.7 2.5 ] supported_automake=[ 1.16 ] ]
require option-renames [ renames=[ 'gnutls providers:gnutls' ] ]

SUMMARY="An asynchronous XMLPP library"

LICENCES="LGPL-2.1"
SLOT="1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/check[>=0.9.2]
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/glib:2[>=2.38.0]
        dev-libs/libtasn1[>=0.2.6]
        dev-libs/libxml2:2.0
        net-dns/libidn
        net-libs/libasyncns[>=0.3]
        providers:gnutls? ( dev-libs/gnutls[>=3.0.20] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}/${PNV}-disable-invalid-tests-test.patch"
)

src_configure() {
    local args=()

    args+=(
        # Avoid -Werror
        --with-compile-warnings=yes

        --enable-idn
        --with-asyncns=system

        --disable-gtk-doc
    )

    local ssl='openssl'
    option providers:gnutls && ssl='gnutls'

    args+=(
        --with-ssl=${ssl}
    )

    econf "${args[@]}"
}

