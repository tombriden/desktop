# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2009 Daniel Mierswa <impulze@impulze.org>
# Copyright 2015 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MOZ_CODENAME="Daily"

linguas=(
    af ar ast be bg br ca cak cs cy da de dsb el en_CA en_GB en_US es_AR es_ES et eu fa fi fr fy_NL
    ga_IE gd gl he hr hsb hu hy_AM id is it ja ka kab kk ko lt ms nb_NO nl nn_NO pa_IN pl pt_BR
    pt_PT rm ro ru si sk sl sq sr sv_SE th tr uk uz vi zh_CN zh_TW
)

# autoconf:2.5 doesn't work - https://bugzilla.mozilla.org/show_bug.cgi?id=104642
require autotools [ supported_autoconf=[ 2.1 ] supported_automake=[ 1.16 1.15 1.13 1.12 ] ]
require mozilla-pgo [ co_project=comm/mail codename="${MOZ_CODENAME}" ]
# FIXME: The bindist option as handled by mozilla.exlib doesn't work with current
# Thunderbirds anymore. I couldn't find out how to do it these days either so for
# now it's gone.
require toolchain-funcs utf8-locale

SUMMARY="Mozilla's standalone mail and news client"
HOMEPAGE="https://www.mozilla.com/en-US/${PN}/"
DOWNLOADS="https://ftp.mozilla.org/pub/${PN}/releases/${PV}/source/${PNV}.source.tar.xz"
for lang in "${linguas[@]}" ; do
    DOWNLOADS+="
        linguas:${lang}? ( https://ftp.mozilla.org/pub/${PN}/releases/${PV}/linux-x86_64/xpi/${lang/_/-}.xpi -> ${PNV}-${lang}.xpi )
    "
done

REMOTE_IDS="freshcode:${PN}"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}${PV}/releasenotes/"

LICENCES="MPL-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    accessibility
    libproxy    [[ description = [ Use libproxy for system proxy settings ] ]]
    necko-wifi  [[ description = [ Scan WiFi with its internal network library ] ]]
    pulseaudio
    wayland   [[ description = [ Use wayland as the default backend ] ]]

    ( linguas: ${linguas[@]} )
    ( providers: jpeg-turbo )
"

DEPENDENCIES="
    build:
        app-arch/zip
        dev-lang/nasm
        dev-lang/clang:*[>=8.0]
        dev-lang/perl:*[>=5.0]
        dev-lang/python:*[>=3.6][sqlite]
        dev-python/ply
        dev-rust/cbindgen[>=0.24.0]
        virtual/pkg-config
        virtual/unzip
    build+run:
        app-spell/hunspell:=
        dev-lang/llvm:=[>=8.0]
        dev-lang/node
        dev-lang/rust:*[>=1.51]
        dev-libs/botan:=[>=2.8.0]
        dev-libs/glib:2[>=2.42]
        dev-libs/dbus-glib:1
        dev-libs/icu:=[>=71.1]
        dev-libs/libevent:=
        dev-libs/libffi:=[>=3.0.9]
        dev-libs/nspr[>=4.32]
        media-libs/fontconfig[>=2.7.0]
        media-libs/freetype:2[>=2.2.0] [[ note = [ aka 9.10.3 ] ]]
        media-libs/libvpx:=[>=1.8.0]
        media-libs/libwebp:=[>=1.0.2]
        sys-apps/dbus[>=0.60]
        sys-libs/zlib[>=1.2.3]
        sys-sound/alsa-lib
        x11-dri/libdrm[>=2.4]
        x11-libs/cairo[X][>=1.10]
        x11-libs/gdk-pixbuf:2.0[>=2.26.5]
        x11-libs/gtk+:3[>=3.22.0][wayland]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXext
        x11-libs/libxkbcommon[>=0.4.1]
        x11-libs/libXrandr[>=1.4.0]
        x11-libs/libXrender
        x11-libs/libXScrnSaver
        x11-libs/libXt
        x11-libs/pango[>=1.22][X(+)]
        x11-libs/pixman:1[>=0.36.0]
        libproxy? ( net-libs/libproxy:1 )
        necko-wifi? ( net-wireless/wireless_tools )
        !pgo? ( dev-libs/nss[>=3.79.2] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        pulseaudio? ( media-sound/pulseaudio )
    suggestion:
        x11-libs/libnotify[>=0.4] [[ description = [ Show notifications with libnotify ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-non-fatal-pip-check.patch
)

MOZILLA_SRC_CONFIGURE_PARAMS=(
    # Experimental
    --disable-icu4x
    --disable-install-strip
    # rustc hangs when enabled during building the jsparagus crate
    --disable-smoosh
    --disable-strip
    # webrtc fails to build, missing pk11pub.h
    --disable-webrtc
    # HTML5 media support
    --enable-alsa
    --enable-dbus
    --enable-rust-simd
    --enable-webspeech

    --enable-system-pixman
    --with-system-ffi
    --with-system-botan
    --with-system-icu
    --with-system-libevent
    --with-system-libvpx
    #--with-system-png # needs libpng with APNG
    --with-system-webp
    --with-system-zlib
    # Probably a good idea to enable this at some point, but we would need
    # wasi-sdk (https://github.com/WebAssembly/wasi-sdk)
    --without-wasm-sandboxed-libraries

    # v requires sqlite built with -DSQLITE_SECURE_DELETE
    # v and enabling it resulted in linker issues
    #--enable-system-sqlite

    # optimizations
    --disable-crashreporter
    --disable-simulator
    --disable-updater

    --enable-hardening
    --enable-sandbox
)

MOZILLA_SRC_CONFIGURE_OPTIONS=(
    # pgo fails to build with system nss: "x86_64-pc-linux-gnu-ld: error:
    # cannot find -lnssutil3"
    'debug --without-system-nss'
    'pgo --without-system-nss'
)

MOZILLA_SRC_CONFIGURE_OPTION_ENABLES=(
    accessibility
    libproxy
    necko-wifi
)
MOZILLA_SRC_CONFIGURE_OPTION_WITHS=(
    # --with-system-jpeg needs libjpeg-turbo or whatever provides JCS_EXTENSIONS
    'providers:jpeg-turbo system-jpeg'
)

pkg_pretend() {
    if has_version ${CATEGORY}/${PN}[bindist]; then
        ewarn "The bindist option had to be hard-disabled for ${CATEGORY}/${PNV}. Your current installation"
        ewarn "has this option enabled. You should be aware that you must not distribute builds"
        ewarn "without the bindist option. If this is a problem for you, fix the exheres."
    fi
}

src_unpack() {
    default

    for lang in "${linguas[@]}" ; do
        if option linguas:${lang} ; then
            edo mkdir "${WORKBASE}"/${lang}
            edo unzip -qo "${FETCHEDDIR}"/${PNV}-${lang}.xpi -d "${WORKBASE}"/${lang}
        fi
    done
}

src_prepare() {
    require_utf8_locale

    export TOOLCHAIN_PREFIX=$(exhost --tool-prefix)

    # those tests seem to fail in non-UTC timezone
    edo rm js/src/jit-test/tests/sunspider/check-date-format-{xparb,tofte}.js

    mozilla-pgo_src_prepare

    edo sed -i -e "s:objdump:${TOOLCHAIN_PREFIX}&:" python/mozbuild/mozbuild/configure/check_debug_ranges.py

    edo sed -i -e "s:objdump:\$(TOOLCHAIN_PREFIX)&:g" Makefile.in

    # Fix build with cbindgen[>=0.24], short hack instead of
    # https://hg.mozilla.org/mozilla-central/rev/51947744ce12, which will be
    # at least in firefox 103.0
    edo sed -e "/const uint64_t ROOT_CLIP_CHAIN = ~0;/d" \
        -i gfx/webrender_bindings/webrender_ffi.h

    edo mkdir "${TEMP}/mozbuild"

    edo sed -e "/pypi-optional:psutil/s/5.8.0/5.9.0/" -i python/sites/mach.txt

    eautoreconf
}

src_configure() {
    # x86_64-pc-linux-gnu-as: invalid option -- 'N'
    export AS=$(exhost --tool-prefix)cc

    if cxx-is-gcc; then
        botan_compiler=gcc
    elif cxx-is-clang; then
        botan_compiler=clang
    else
        die "Unknown compiler ${CXX}; you will need to add a check for it to the thunderbird exheres"
    fi

    local audio_backends="alsa"
    option pulseaudio && audio_backends+=",pulseaudio"

    export MOZBUILD_STATE_PATH="${TEMP}/mozbuild"

    esandbox allow_net --connect "unix:/run/uuidd/request"
    mozilla-pgo_src_configure \
        EXHERBO_BOTAN_CC=${botan_compiler} \
        $(cc-is-clang || echo "--with-clang-path=/usr/$(exhost --build)/bin/$(exhost --tool-prefix)clang") \
        $(ld-is-lld && echo "--disable-elf-hack") \
        --enable-audio-backends=${audio_backends} \
        --enable-default-toolkit=cairo-gtk3$(option wayland -wayland)
    esandbox disallow_net --connect "unix:/run/uuidd/request"
}

src_install() {
    mozilla-pgo_src_install

    # allow installation of distributed extensions and read/use system locale on runtime
    insinto /usr/$(exhost --target)/lib/${PN}/defaults/pref
    hereins all-exherbo.js <<EOF
pref("extensions.autoDisableScopes", 3);
pref("intl.locale.requested", "");
EOF

    for lang in "${linguas[@]}" ; do
        if option linguas:${lang} ; then
            # Extract an id from the localisation tarball, usually something
            # like langpack-${lang}@thunderbird.mozilla.org. Not entirely sure
            # which rules it follows so I go with cargo-cult for now.
            emid="$(sed -n -e 's/.*"id": "\(.*\)",/\1/p' "${WORKBASE}"/${lang}/manifest.json | xargs)"
            insinto /usr/$(exhost --target)/lib/${PN}/distribution/extensions
            newins "${FETCHEDDIR}"/${PNV}-${lang}.xpi ${emid}.xpi
        fi
    done
}

